package com.ins1st.core.aspect;

import com.ins1st.core.annotation.NeedAuth;
import com.ins1st.util.ShiroUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-19 14:46
 **/
@Aspect
@Component
public class NeedAuthAspect {

    @Pointcut("@annotation(com.ins1st.core.annotation.NeedAuth)")
    private void cut() {
    }

    @Before("cut()")
    public void before(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        NeedAuth needAuth = method.getAnnotation(NeedAuth.class);
        String value = needAuth.value();
        if (StringUtils.isNotBlank(value)) {
            Subject subject = ShiroUtil.getSubject();
            boolean flag = subject.isPermitted(value);
            if (flag == false) {

            }
        }
    }
}
