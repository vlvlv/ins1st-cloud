package com.ins1st.core.handler;

import com.alibaba.fastjson.JSON;
import com.ins1st.base.R;
import com.ins1st.exception.AuthException;
import com.ins1st.exception.BussinessException;
import com.ins1st.exception.NoPermissionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-18 15:40
 **/
@ControllerAdvice
public class ConExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Object exception(Exception e, HttpServletResponse response, HttpServletRequest request) throws Exception {
        e.printStackTrace();
        return handle(request, response, "系统错误,联系管理员", 500);
    }

    @ExceptionHandler(BussinessException.class)
    public Object bussinessException(Exception e, HttpServletResponse response, HttpServletRequest request) throws Exception {
        e.printStackTrace();
        return handle(request, response, e.getMessage(), 500);
    }

    @ExceptionHandler(AuthException.class)
    public Object authException(Exception e, HttpServletResponse response, HttpServletRequest request) {
        e.printStackTrace();
        return handle(request, response, e.getMessage(), 403);
    }

    @ExceptionHandler(NoPermissionException.class)
    public Object noPermissionException(Exception e, HttpServletResponse response, HttpServletRequest request) {
        e.printStackTrace();
        return handle(request, response, "没有权限", 200);
    }


    /**
     * 处理返回值
     *
     * @param request
     * @param response
     * @param mesaage
     * @return
     */
    private Object handle(HttpServletRequest request, HttpServletResponse response, String mesaage, int code) {
        if (isAjax(request)) {
            R r = new R();
            r.setCode(code);
            r.setMessage(mesaage);
            writeJson(response, r);
            return null;
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("/500.html");
            return mav;
        }
    }


    /**
     * 判断属于什么请求
     *
     * @param request
     * @return
     */
    private boolean isAjax(HttpServletRequest request) {
        return (request.getHeader("X-Requested-With") != null
                && "XMLHttpRequest".equals(request.getHeader("X-Requested-With").toString()));
    }

    /**
     * 向response写数据
     *
     * @param response
     * @param r
     */
    protected void writeJson(HttpServletResponse response, R r) {
        try {
            response.setContentType("text/html;charset=UTF-8");
            response.getWriter().write(JSON.toJSONString(r));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
