package com.ins1st;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients
public class Ins1stWebApplication {

    private static final Logger log = LoggerFactory.getLogger(Ins1stWebApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(Ins1stWebApplication.class, args);
        log.info(Ins1stWebApplication.class.getSimpleName() + " is start success");
    }

}
