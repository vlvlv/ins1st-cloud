package com.ins1st.modular.log;

import com.ins1st.api.SysLoginLogApi;
import com.ins1st.entity.SysLoginLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 14:09
 **/
@RequestMapping(value = "/log/login")
@Controller
public class SysLoginLogController {

    private static final String MODEL = "pages/log/login/";

    @Autowired
    private SysLoginLogApi sysLoginLogApi;

    @RequestMapping(value = "/index")
    public String index() {
        return MODEL + "login_index.html";
    }

    @RequestMapping(value = "/page")
    @ResponseBody
    public Object page(SysLoginLog sysLoginLog) {
        return this.sysLoginLogApi.page(sysLoginLog);
    }


}
