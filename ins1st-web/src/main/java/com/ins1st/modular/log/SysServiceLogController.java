package com.ins1st.modular.log;

import com.ins1st.api.SysServiceLogApi;
import com.ins1st.entity.SysServiceLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 14:09
 **/
@RequestMapping(value = "/log/service")
@Controller
public class SysServiceLogController {

    private static final String MODEL = "pages/log/service/";

    @Autowired
    private SysServiceLogApi sysServiceLogApi;

    @RequestMapping(value = "/index")
    public String index() {
        return MODEL + "service_index.html";
    }

    @RequestMapping(value = "/page")
    @ResponseBody
    public Object page(SysServiceLog sysServiceLog) {
        return this.sysServiceLogApi.page(sysServiceLog);
    }


}
