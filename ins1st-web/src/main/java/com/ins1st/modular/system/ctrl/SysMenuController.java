package com.ins1st.modular.system.ctrl;

import com.ins1st.api.SysMenuApi;
import com.ins1st.base.R;
import com.ins1st.entity.SysMenu;
import com.ins1st.tree.Ztree;
import com.ins1st.util.QiNuiUpload;
import com.ins1st.util.ShiroUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-11 10:27
 **/
@Controller
@RequestMapping(value = "/sys/menu")
public class SysMenuController {

    private static final String MODEL = "pages/sys/menu/";

    @Autowired
    private SysMenuApi sysMenuApi;
    @Autowired
    private QiNuiUpload qiNuiUpload;


    @RequestMapping(value = "/test")
    @ResponseBody
    public Object test() {
        return sysMenuApi.queryIndexMenus(ShiroUtil.getUser().getId());
    }

    /**
     * 主页
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/index")
    public String index(Model model) {
        return MODEL + "menu_index.html";
    }

    /**
     * 查询树结构
     *
     * @return
     */
    @RequestMapping(value = "/selectZtree")
    @ResponseBody
    public Object selectZtree(String roleId) {
        R r = this.sysMenuApi.selectZtree(roleId);
        return r;
    }

    /**
     * 查询列表
     *
     * @return
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(SysMenu sysMenu) {
        R r = this.sysMenuApi.list(sysMenu);
        r.setCode(0);
        return r;
    }

    /**
     * 查询列表
     *
     * @return
     */
    @RequestMapping(value = "/noUrl")
    @ResponseBody
    public Object noUrl(SysMenu sysMenu) {
        R r = this.sysMenuApi.list(sysMenu);
        List<Ztree> ztreeList = new ArrayList<>();
        for (SysMenu sm : (List<SysMenu>) r.getData()) {
            Ztree ztree = new Ztree();
            BeanUtils.copyProperties(sm, ztree);
            ztreeList.add(ztree);
        }
        r.setData(ztreeList);
        return r;
    }

    /**
     * 保存用户和角色
     *
     * @param roleId
     * @param menuIds
     * @return
     */
    @RequestMapping(value = "/saveUserRoles")
    @ResponseBody
    public Object saveUserRoles(String roleId, String menuIds) {
        R r = this.sysMenuApi.saveRoleMenus(roleId, menuIds);
        return r;
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String id) {
        R r = this.sysMenuApi.del(id);
        return r;
    }


    /**
     * 添加页
     *
     * @return
     */
    @RequestMapping(value = "/add")
    public String add() {
        return MODEL + "menu_add.html";
    }

    /**
     * 添加页
     *
     * @return
     */
    @RequestMapping(value = "/edit")
    public String edit(String id, Model model) {
        R<SysMenu> r = this.sysMenuApi.selectOne(id);
        model.addAttribute("sysMenu", r.getData());
        return MODEL + "menu_edit.html";
    }

    /**
     * 保存或更新
     *
     * @param sysMenu
     * @return
     */
    @RequestMapping(value = "/insertOrUpdate")
    @ResponseBody
    public Object insertOrUpdate(SysMenu sysMenu) {
        R r = this.sysMenuApi.insertOrUpdate(sysMenu);
        return r;
    }
}
