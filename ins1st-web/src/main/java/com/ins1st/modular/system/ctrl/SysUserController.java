package com.ins1st.modular.system.ctrl;

import com.ins1st.api.SysUserApi;
import com.ins1st.base.R;
import com.ins1st.core.annotation.NeedAuth;
import com.ins1st.entity.SysUser;
import com.ins1st.util.ShiroUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-13 13:19
 **/
@RequestMapping(value = "/sys/user")
@Controller
public class SysUserController {

    private static final String MODEL = "pages/sys/user/";

    @Autowired
    private SysUserApi sysUserApi;

    /**
     * 主页
     *
     * @return
     */
    @RequestMapping(value = "/index")
    @NeedAuth(value = "sys:user:index")
    public String index() {
        return MODEL + "user_index.html";
    }


    /**
     * 分页
     *
     * @param sysUser
     * @return
     */
    @RequestMapping(value = "/page")
    @ResponseBody
    public Object page(SysUser sysUser) {
        return sysUserApi.page(sysUser);
    }


    /**
     * 更新
     *
     * @param sysUser
     * @return
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(SysUser sysUser) {
        R r = this.sysUserApi.insertOrUpdate(sysUser);
        return r;
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(String id) {
        R r = this.sysUserApi.remove(id);
        return r;
    }

    /**
     * 添加页
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/add")
    public String add(Model model) {
        return MODEL + "user_add.html";
    }

    /**
     * 添加页
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/edit")
    public String edit(Model model, String id) {
        SysUser sysUser = new SysUser();
        sysUser.setId(id);
        R<SysUser> r = this.sysUserApi.selectOne(sysUser);
        sysUser = (SysUser) r.getData();
        model.addAttribute("sysUser", sysUser);
        return MODEL + "user_edit.html";
    }


    /**
     * 保存或更新
     *
     * @param sysUser
     * @return
     */
    @RequestMapping(value = "/insertOrUpdate")
    @ResponseBody
    public Object insertOrUpdate(SysUser sysUser) {
        R r = this.sysUserApi.insertOrUpdate(sysUser);
        if (StringUtils.isNotBlank(sysUser.getId()) && sysUser.getId().equals(ShiroUtil.getUser().getId())) {
            SysUser newUser = new SysUser();
            newUser.setId(sysUser.getId());
            newUser = this.sysUserApi.selectOne(newUser).getData();
            ShiroUtil.updateUser(sysUser);
        }
        return r;
    }


    /**
     * 个人中心
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/myInfo")
    public String myInfo(Model model) {
        SysUser sysUser = ShiroUtil.getUser();
        model.addAttribute("sysUser", sysUser);
        return MODEL + "my_info.html";
    }

}
