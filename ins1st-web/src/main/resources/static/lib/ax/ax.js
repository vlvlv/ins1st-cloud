/**
 * 简易再一次封装ajax
 */
layui.define(['jquery'], function (exports) {
    var $ = layui.$;

    function $ax(url, successFunc) {
        this.url = url;
        this.params = {};

        /**
         * 设值
         * @param key
         * @param value
         */
        this.set = function (key, value) {
            if (typeof key === "object") {
                this.params = key;
            } else {
                if (this.params[key] != null || this.params[key] != "") {
                    delete this.params[key];
                }
                this.params[key] = value;
            }
        };

        /**
         * 直接用表单提交
         * @param formId
         */
        this.postForm = function (data) {
            this.params = data
        };
        /**
         * 开始请求
         */
        this.start = function () {
            $.ajax({
                'url': this.url,//请求的url
                'type': "POST",//请求的类型
                'data': this.params,//请求的参数
                'dataType': "json",//接收数据类型
                'async': true,//异步请求
                'cache': false,//浏览器缓存
                'success': function (result) {
                    success(result);
                }
            })
        };

        /**
         * 请求成功方法
         * @param result
         */
        function success(result) {
            if (successFunc != null || successFunc != undefined) {
                successFunc(result);
            }
        }

    };
    exports('ax', $ax);
});