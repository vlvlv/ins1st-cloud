layui.use(['table', 'ax', 'form'], function () {
    let table = layui.table;
    let $ax = layui.ax;
    let form = layui.form;

    table.render({
        elem: '#tableId',
        url: ctx + '/sys/role/page',
        limit: 20,
        size: "sm",
        page: true,
        toolbar: true,
        toolbar: "#toolbarTpl",
        cols: [[
            {type: "radio", fixed: "left"},
            {field: "id", title: "ID", width: 100, sort: true},
            {field: "name", title: "角色名称", width: 150},
            {field: "keyword", title: "角色标识", width: 150},
            {
                field: "status", title: "状态", width: 150, templet: function (d) {
                    if (d.status == '0') {
                        return '<span class="layui-btn layui-btn-normal layui-btn-xs">正常</span>';
                    }
                    return '<span class="layui-btn layui-btn-danger layui-btn-xs">禁用</span>';
                }
            },
            {field: "createTime", title: "创建时间", width: 150},
            {title: "操作", align: "center", fixed: "right", templet: "#operationTpl"}
        ]]
    });

    form.on("submit(search)", function () {
        let queryData = {};
        queryData['name'] = $("#name").val();
        table.reload("tableId", {
            where: queryData, page: {curr: 1}
        });
        return false;
    });

    table.on("tool(tableFilter)", function (obj) {
        let data = obj.data;
        switch (obj.event) {
            case "del":
                admin.confirm("确定删除该角色?", function () {
                    var ax = new $ax(ctx + "/sys/role/delete", function (result) {
                        admin.success(result.message, function () {
                            table.reload("tableId");
                        });
                    });
                    ax.set("id", data.id);
                    ax.start();
                });
                break;
            case "edit":
                admin.open("修改角色", ctx + "/sys/role/edit?id=" + obj.data.id, "90%", "90%", null, function () {
                    table.reload("tableId");
                });
                break;
        }
    });

    table.on("toolbar(tableFilter)", function (obj) {
        let checked = table.checkStatus('tableId').data;
        switch (obj.event) {
            case "add" :
                admin.open("添加角色", ctx + "/sys/role/add", "90%", "90%", null, function () {
                    table.reload("tableId");
                });
                break;
            case "enabled":
                if (checked.length == 0) {
                    admin.error("请先选中一条数据。");
                    return false;
                }
                admin.confirm("确定启动该用户?", function () {
                    var ax = new $ax(ctx + "/sys/role/insertOrUpdate", function (result) {
                        admin.success(result.message, function () {
                            table.reload("tableId");
                        });
                    });
                    ax.set("id", checked[0].id);
                    ax.set("status", "0");
                    ax.start();
                });
                break;
            case "disabled":
                if (checked.length == 0) {
                    admin.error("请先选中一条数据。");
                    return false;
                }
                admin.confirm("确定禁止该用户?", function () {
                    var ax = new $ax(ctx + "/sys/role/insertOrUpdate", function (result) {
                        admin.success(result.message, function () {
                            table.reload("tableId");
                        });
                    });
                    ax.set("id", checked[0].id);
                    ax.set("status", "1");
                    ax.start();
                });
                break;
            case "menu":
                if (checked.length == 0) {
                    admin.error("请先选中一条记录");
                    return false;
                }
                var ax = new $ax(ctx + "/sys/menu/selectZtree?roleId=" + checked[0].id, function (result) {
                    let index = layer.open({
                        type: 1,
                        btn: ["确定", "取消"],
                        yes: function () {
                            let checkedNodes = $.fn.zTree.getZTreeObj("tree").getCheckedNodes();
                            let ids = "";
                            for (let i = 0; i < checkedNodes.length; i++) {
                                ids = ids + checkedNodes[i].id + ",";
                            }
                            var ax2 = new $ax(ctx + "/sys/menu/saveUserRoles", function (result) {
                                if (result.success) {
                                    admin.success("分配成功");
                                    layer.close(index);
                                }
                            });
                            ax2.set("roleId", checked[0].id);
                            ax2.set("menuIds", ids);
                            ax2.start();
                        },
                        title: "分配角色",
                        area: ['300px', '350px'],
                        content: "<div><ul id='tree' class='ztree'></ul></div>",
                    });
                    $.fn.zTree.init($("#tree"), setting, result.data);
                    $.fn.zTree.getZTreeObj("tree").expandAll(true);
                });
                ax.start();
                break;
        }
    })

    let setting = {
        check: {
            enable: true,
            chkStyle: "checkbox",    //复选框
            chkboxType: {
                "Y": "ps",
                "N": "ps"
            }
        },
        data: {
            simpleData: {
                enable: true,//是否采用简单数据模式
                idKey: "id",//树节点ID名称
                pIdKey: "pId",//父节点ID名称
                rootPId: null,//根节点ID
            }
        }
    };
});