layui.use(['form', 'ax'], function () {
    let form = layui.form;
    let $ax = layui.ax;

    form.on("submit(add)", function (data) {
        var ax = new $ax(ctx + "/sys/menu/insertOrUpdate", function (result) {
            admin.success(result.message, function () {
                admin.close();
            });
        });
        ax.postForm(data.field);
        ax.start();
        return false;
    });
    let setting = {
        data: {
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "pId",
                rootPId: 0,
            }
        },
        callback: {
            onClick: function (event, treeId, treeNode) {
                $("#pName").val(treeNode.name);
                $("#pId").val(treeNode.id);
            }
        }
    };

    $("#pName").click(function () {
        var ax1 = new $ax(ctx + "/sys/menu/noUrl", function (result) {
            let index = layer.open({
                type: 1,
                btn: ["确定", "取消"],
                yes: function () {
                    layer.close(index);
                },
                title: "分配角色",
                area: ['300px', '350px'],
                content: "<div><ul id='tree' class='ztree'></ul></div>",
            });
            var json = {"id": "0", "pId": "", "name": "顶级菜单"};

            result.data.push(JSON.parse(JSON.stringify(json)));
            $.fn.zTree.init($("#tree"), setting, result.data);
            if ($("#pId").val() != null && $("#pId").val() != '') {
                var node = $.fn.zTree.getZTreeObj("tree").getNodeByParam("id", $("#pId").val());
                $.fn.zTree.getZTreeObj("tree").selectNode(node, true);
            }
            $.fn.zTree.getZTreeObj("tree").expandAll(true);

        });
        ax1.start();

    });
})
;