let ctx = "";
let loadIndex;
let admin = {
    addCtx: function (path) {
        ctx = path;
    },
    confirm: function (content, yesFunction) {
        let options = {skin: "layui-layer-lan", icon: 3, title: "提示", anim: 0};
        layer.confirm(content, options, yesFunction);
    },
    open: function (title, content, width, height, successFunction, endFunction) {
        layer.open({
            title: title,
            type: 2,
            maxmin: true,
            shade: 0.5,
            anim: 0,
            area: [width, height],
            content: content,
            zIndex: layer.zIndex,
            skin: "layui-layer-lan",
            success: successFunction,
            end: endFunction
        });
    },
    close: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    },
    success: function (message, func, func2) {
        var index = layer.alert(message, {
            icon: 1,
            btn: ['确定'],
            yes: function () {
                layer.close(index);
                if (func != null) {
                    func();
                }
            },
            end: function () {
                if (func2 != null) {
                    func2();
                }
            }
        });
    },
    error: function (message, func, func2) {
        var index = layer.alert(message, {
            icon: 2,
            btn: ['确定'],
            yes: function () {
                layer.close(index);
                if (func != null) {
                    func();
                }
            },
            end: function () {
                if (func2 != null) {
                    func2();
                }
            }
        });
    },
    show: function () {
        loadIndex = layer.load(2, {shade: [0.5, '#fff']});
    },
    hide: function () {
        layer.close(loadIndex);
    }
};

layui.config({
    base: ctx + '/static/lib/'
}).extend({
    "ax": "ax/ax",
    "treeGrid": "treeGrid/treeGrid",
    "eleTree": "eleTree/eleTree",
}).use([], function () {
    $.ajaxSetup({
        complete: function (xhr) {
            let json = JSON.parse(xhr.responseText);
            if (json.code == 500) {
                admin.error(json.message);
            } else if (json.code == 403) {
                admin.error(json.message, function () {
                    window.location.href = ctx + "/login";
                });
            }
            admin.hide();
        }
    });
});





