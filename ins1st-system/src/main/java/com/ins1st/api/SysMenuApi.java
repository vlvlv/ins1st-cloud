package com.ins1st.api;

import com.ins1st.base.R;
import com.ins1st.entity.SysMenu;
import com.ins1st.exception.BussinessException;
import com.ins1st.tree.Ztree;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "ins1st-system", contextId = "SysMenuApi")
public interface SysMenuApi {

    @RequestMapping(value = "/menu/queryIndexMenus", method = RequestMethod.POST)
    R queryIndexMenus(@RequestParam("userId") String userId);

    @RequestMapping(value = "/menu/selectZtree", method = RequestMethod.POST)
    R<List<Ztree>> selectZtree(@RequestParam(value = "roleId", required = false) String roleId);

    @RequestMapping(value = "/menu/saveRoleMenus", method = RequestMethod.POST)
    R saveRoleMenus(@RequestParam("roleId") String roleId, @RequestParam("menuIds") String menuIds);

    @RequestMapping(value = "/menu/list", method = RequestMethod.POST)
    R<List<SysMenu>> list(@RequestBody SysMenu sysMenu);

    @RequestMapping(value = "/menu/del", method = RequestMethod.POST)
    R del(@RequestParam("id") String id);

    @RequestMapping(value = "/menu/selectOne", method = RequestMethod.POST)
    R<SysMenu> selectOne(@RequestParam("id") String id);

    @RequestMapping(value = "/menu/insertOrUpdate", method = RequestMethod.POST)
    R insertOrUpdate(@RequestBody SysMenu sysMenu);

    @RequestMapping(value = "/menu/queryMenusByUserId", method = RequestMethod.POST)
    R<List<SysMenu>> queryMenusByUserId(@RequestParam("id") String id);

}
