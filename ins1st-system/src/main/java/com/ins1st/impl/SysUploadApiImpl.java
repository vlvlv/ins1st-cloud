package com.ins1st.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ins1st.api.SysUploadApi;
import com.ins1st.base.R;
import com.ins1st.entity.SysUpload;
import com.ins1st.mapper.SysUploadMapper;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-21 15:54
 **/
@RestController
public class SysUploadApiImpl extends ServiceImpl<SysUploadMapper, SysUpload> implements SysUploadApi {


    @Override
    public R<SysUpload> insert(SysUpload sysUpload) {
        this.save(sysUpload);
        return R.success(sysUpload);
    }
}
