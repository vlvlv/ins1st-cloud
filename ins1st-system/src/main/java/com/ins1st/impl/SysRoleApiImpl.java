package com.ins1st.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ins1st.api.SysRoleApi;
import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysRole;
import com.ins1st.entity.SysUser;
import com.ins1st.entity.SysUserRole;
import com.ins1st.mapper.SysRoleMapper;
import com.ins1st.mapper.SysUserRoleMapper;
import com.ins1st.tree.Ztree;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-14 13:10
 **/
@RestController
@Transactional
public class SysRoleApiImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleApi {

    @Autowired
    SysUserRoleMapper sysUserRoleMapper;

    @Override
    public PageResult page(SysRole sysRole) {
        QueryWrapper qw = new QueryWrapper();
        qw.like(StringUtils.isNotBlank(sysRole.getName()), "name", sysRole.getName());
        IPage<SysUser> page = this.baseMapper.selectPage(new Page(sysRole.getPage(), sysRole.getLimit()), qw);
        return R.page(page);
    }

    @Override
    public R insertOrUpdate(SysRole sysRole) {
        if (StringUtils.isNotBlank(sysRole.getId())) {
            this.baseMapper.updateById(sysRole);
        } else {
            sysRole.setCreateTime(new Date());
            this.baseMapper.insert(sysRole);
        }
        return R.success("操作成功");
    }

    @Override
    public R remove(String id) {
        this.baseMapper.deleteById(id);
        return R.success("操作成功");
    }

    @Override
    public R<SysRole> selectOne(String id) {
        SysRole sysRole = this.baseMapper.selectById(id);
        return R.success(sysRole);
    }

    @Override
    public R<List<Ztree>> selectZtree(String userId) {
        List<SysRole> sysRoleList = this.baseMapper.selectList(null);
        List<SysUserRole> userRoles = this.sysUserRoleMapper.selectList(new QueryWrapper<SysUserRole>().eq("user_id", userId));
        List<Ztree> ztreeList = new ArrayList<>();
        for (SysRole sysRole : sysRoleList) {
            Ztree ztree = new Ztree();
            ztree.setId(sysRole.getId());
            ztree.setpId("0");
            ztree.setName(sysRole.getName());
            for (SysUserRole sysUserRole : userRoles) {
                if (sysUserRole.getRoleId().equals(sysRole.getId())) {
                    ztree.setChecked(true);
                }
            }
            ztreeList.add(ztree);
        }
        return R.success(ztreeList);
    }


    @Override
    public R saveUserRoles(String userId, String roleIds) {
        this.sysUserRoleMapper.delete(new QueryWrapper<SysUserRole>().eq("user_id", userId));
        String[] ids = roleIds.split(",");
        for (String id : ids) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setUserId(userId);
            sysUserRole.setRoleId(id);
            this.sysUserRoleMapper.insert(sysUserRole);
        }
        return R.success("操作成功");
    }
}
