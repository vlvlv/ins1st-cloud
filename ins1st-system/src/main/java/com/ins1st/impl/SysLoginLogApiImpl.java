package com.ins1st.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ins1st.api.SysLoginLogApi;
import com.ins1st.base.PageResult;
import com.ins1st.base.R;
import com.ins1st.entity.SysLoginLog;
import com.ins1st.mapper.SysLoginLogMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-20 13:51
 **/
@RestController
@Transactional
public class SysLoginLogApiImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements SysLoginLogApi {

    @Override
    public PageResult page(SysLoginLog sysLoginLog) {
        QueryWrapper qw = new QueryWrapper();
        qw.like(StringUtils.isNotBlank(sysLoginLog.getLoginName()), "login_name", sysLoginLog.getLoginName());
        IPage<SysLoginLog> page = this.baseMapper.selectPage(new Page(sysLoginLog.getPage(), sysLoginLog.getLimit()), qw);
        return R.page(page);
    }

    @Override
    public R insert(SysLoginLog sysLoginLog) {
        this.save(sysLoginLog);
        return R.success();
    }
}
