package com.ins1st.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ins1st.entity.SysRoleMenu;

public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {
}
