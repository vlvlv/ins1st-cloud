package com.ins1st.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ins1st.entity.SysMenu;

import java.util.List;

public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> queryMenusByUserId(String id);
}
