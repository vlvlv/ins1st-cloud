package com.ins1st.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ins1st.entity.SysRole;

public interface SysRoleMapper extends BaseMapper<SysRole> {
}
