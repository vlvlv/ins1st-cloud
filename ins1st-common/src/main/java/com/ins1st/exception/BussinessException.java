package com.ins1st.exception;

import com.netflix.hystrix.exception.HystrixBadRequestException;

/**
 * @program: ins1st-cloud
 * @description:
 * @author: coderSun
 * @create: 2019-11-18 15:24
 **/
public class BussinessException extends HystrixBadRequestException {

    public BussinessException(String message) {
        super(message);
    }

    public BussinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
