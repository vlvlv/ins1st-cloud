/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : ins1st-cloud

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 21/11/2019 17:48:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_constants
-- ----------------------------
DROP TABLE IF EXISTS `sys_constants`;
CREATE TABLE `sys_constants` (
  `id` varchar(64) NOT NULL,
  `constants_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量名称',
  `constants_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量key',
  `constants_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量value',
  `constants_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '变量value',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数配置';

-- ----------------------------
-- Records of sys_constants
-- ----------------------------
BEGIN;
INSERT INTO `sys_constants` VALUES ('1181208349362913282', '系统名称', 'INS1ST_SYSTEM_NAME', 'ins1st-cloud v1.0 | 很赞的后台管理系统', '默认系统名称');
INSERT INTO `sys_constants` VALUES ('1181208349362913283', '码云地址', 'GITEE_URL', 'https://gitee.com/sdjwj1118/ins1st-plus', '码云项目地址');
INSERT INTO `sys_constants` VALUES ('1181208349362913284', '关键词', 'KEYWORDS', 'ins1st-cloud v1.0,基于SpringBoot、SpringCloud、SpringCloudAlibaba分布式开发框架', '关键词');
COMMIT;

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `login_ip` varchar(30) DEFAULT NULL,
  `login_success` char(1) DEFAULT NULL,
  `login_result` varchar(50) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_login_log` VALUES ('1197043331775873025', 'admin', '0:0:0:0:0:0:0:1', '1', '登入成功', '2019-11-20 06:45:45');
INSERT INTO `sys_login_log` VALUES ('1197081441918271489', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-20 09:17:11');
INSERT INTO `sys_login_log` VALUES ('1197081807326035970', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-20 09:18:38');
INSERT INTO `sys_login_log` VALUES ('1197081910224896002', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-20 09:19:02');
INSERT INTO `sys_login_log` VALUES ('1197082252354273281', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-20 09:20:24');
INSERT INTO `sys_login_log` VALUES ('1197082562137178114', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-20 09:21:38');
INSERT INTO `sys_login_log` VALUES ('1197083960534908930', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-20 09:27:11');
INSERT INTO `sys_login_log` VALUES ('1197087131873329154', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-20 09:39:47');
INSERT INTO `sys_login_log` VALUES ('1197345808299610114', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-21 02:47:40');
INSERT INTO `sys_login_log` VALUES ('1197346006476279809', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-21 02:48:06');
INSERT INTO `sys_login_log` VALUES ('1197346028471209985', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-21 02:48:06');
INSERT INTO `sys_login_log` VALUES ('1197347069581385730', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-21 02:52:41');
INSERT INTO `sys_login_log` VALUES ('1197446961737887745', 'admin', '127.0.0.1', '1', '登入成功', '2019-11-21 09:29:37');
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `p_id` varchar(64) DEFAULT NULL COMMENT '父级菜单',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单地址',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '菜单权限',
  `is_menu` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是不是菜单',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单图标(只限一级菜单使用)',
  `sort` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', '#', NULL, '0', '&#xe6b8;', '4');
INSERT INTO `sys_menu` VALUES ('10', '9', '新增', NULL, 'sys:menu:add', '0', NULL, '1');
INSERT INTO `sys_menu` VALUES ('11', '9', '编辑', NULL, 'sys:menu:edit', '0', NULL, '2');
INSERT INTO `sys_menu` VALUES ('1196756095591370754', '0', '控制台', '/welcome', NULL, '1', '&#xe654;', '1');
INSERT INTO `sys_menu` VALUES ('1196985209589886977', '0', '监控台', '/console', NULL, '1', '&#xe61b;', '2');
INSERT INTO `sys_menu` VALUES ('1196996016964923394', '2', '分配权限', NULL, 'sys:user:role', '0', '', '6');
INSERT INTO `sys_menu` VALUES ('1197018162537721858', '0', '服务监控', 'http://localhost:8769', '', '1', '&#xe7d4;', '3');
INSERT INTO `sys_menu` VALUES ('1197035850223906818', '0', '日志管理', '', '', '0', '&#xe7cb;', '5');
INSERT INTO `sys_menu` VALUES ('1197037164202881025', '1197035850223906818', '登入日志', '/log/login/index', '', '1', NULL, '1');
INSERT INTO `sys_menu` VALUES ('1197037312211480578', '1197035850223906818', '业务日志', '/log/service/index', '', '1', NULL, '2');
INSERT INTO `sys_menu` VALUES ('12', '9', '删除', NULL, 'sys:menu:del', '0', NULL, '3');
INSERT INTO `sys_menu` VALUES ('13', '4', '新增', NULL, 'sys:role:add', '0', NULL, '1');
INSERT INTO `sys_menu` VALUES ('14', '4', '编辑', NULL, 'sys:role:edit', '0', NULL, '2');
INSERT INTO `sys_menu` VALUES ('15', '4', '删除', NULL, 'sys:role:del', '0', NULL, '3');
INSERT INTO `sys_menu` VALUES ('16', '4', '分配菜单', NULL, 'sys:role:menu', '0', NULL, '4');
INSERT INTO `sys_menu` VALUES ('2', '1', '用户管理', '/sys/user/index', NULL, '1', NULL, '1');
INSERT INTO `sys_menu` VALUES ('3', '2', '新增', NULL, 'sys:user:add', '0', NULL, '1');
INSERT INTO `sys_menu` VALUES ('4', '1', '角色管理', '/sys/role/index', 'sys:role:index', '1', NULL, '4');
INSERT INTO `sys_menu` VALUES ('5', '2', '编辑', NULL, 'sys:user:edit', '0', NULL, '2');
INSERT INTO `sys_menu` VALUES ('6', '2', '删除', NULL, 'sys:user:del', '0', NULL, '3');
INSERT INTO `sys_menu` VALUES ('7', '2', '启用', NULL, 'sys:user:enable', '0', NULL, '4');
INSERT INTO `sys_menu` VALUES ('8', '2', '停用', NULL, 'sys:user:disable', '0', NULL, '5');
INSERT INTO `sys_menu` VALUES ('9', '1', '菜单管理', '/sys/menu/index', 'sys:menu:index', '1', NULL, '2');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(64) NOT NULL COMMENT '主键id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `status` varchar(255) DEFAULT NULL COMMENT '角色状态',
  `keyword` varchar(255) DEFAULT NULL COMMENT '角色标识',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES ('1194858296071180290', '超级管理员', '0', 'admin', '2019-11-14');
INSERT INTO `sys_role` VALUES ('1194911442151718914', '测试角色', '0', 'test', '2019-11-14');
INSERT INTO `sys_role` VALUES ('1194911493758435330', '业务员', '0', 'business', '2019-11-14');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `role_id` varchar(64) DEFAULT NULL,
  `menu_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` VALUES ('1196340970904481794', '1194911493758435330', '1');
INSERT INTO `sys_role_menu` VALUES ('1196340970933841922', '1194911493758435330', '2');
INSERT INTO `sys_role_menu` VALUES ('1196340970942230530', '1194911493758435330', '32');
INSERT INTO `sys_role_menu` VALUES ('1197037355853213697', '1194858296071180290', '1');
INSERT INTO `sys_role_menu` VALUES ('1197037355857408001', '1194858296071180290', '2');
INSERT INTO `sys_role_menu` VALUES ('1197037355865796610', '1194858296071180290', '1196996016964923394');
INSERT INTO `sys_role_menu` VALUES ('1197037355874185217', '1194858296071180290', '3');
INSERT INTO `sys_role_menu` VALUES ('1197037355886768130', '1194858296071180290', '5');
INSERT INTO `sys_role_menu` VALUES ('1197037355890962433', '1194858296071180290', '6');
INSERT INTO `sys_role_menu` VALUES ('1197037355895156737', '1194858296071180290', '7');
INSERT INTO `sys_role_menu` VALUES ('1197037355903545345', '1194858296071180290', '8');
INSERT INTO `sys_role_menu` VALUES ('1197037355907739649', '1194858296071180290', '4');
INSERT INTO `sys_role_menu` VALUES ('1197037355911933953', '1194858296071180290', '13');
INSERT INTO `sys_role_menu` VALUES ('1197037355920322562', '1194858296071180290', '14');
INSERT INTO `sys_role_menu` VALUES ('1197037355924516865', '1194858296071180290', '15');
INSERT INTO `sys_role_menu` VALUES ('1197037355928711170', '1194858296071180290', '16');
INSERT INTO `sys_role_menu` VALUES ('1197037355932905474', '1194858296071180290', '9');
INSERT INTO `sys_role_menu` VALUES ('1197037355937099777', '1194858296071180290', '10');
INSERT INTO `sys_role_menu` VALUES ('1197037355941294081', '1194858296071180290', '11');
INSERT INTO `sys_role_menu` VALUES ('1197037355945488385', '1194858296071180290', '12');
INSERT INTO `sys_role_menu` VALUES ('1197037355949682690', '1194858296071180290', '1196756095591370754');
INSERT INTO `sys_role_menu` VALUES ('1197037355953876993', '1194858296071180290', '1196985209589886977');
INSERT INTO `sys_role_menu` VALUES ('1197037355958071297', '1194858296071180290', '1197018162537721858');
INSERT INTO `sys_role_menu` VALUES ('1197037355962265602', '1194858296071180290', '1197035850223906818');
INSERT INTO `sys_role_menu` VALUES ('1197037355966459905', '1194858296071180290', '1197037164202881025');
INSERT INTO `sys_role_menu` VALUES ('1197037355970654209', '1194858296071180290', '1197037312211480578');
COMMIT;

-- ----------------------------
-- Table structure for sys_service_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_service_log`;
CREATE TABLE `sys_service_log` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `log_name` varchar(255) DEFAULT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  `method_params` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `log_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_service_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_service_log` VALUES ('1197073400556699650', 'ins1st-system', '查询用户菜单', 'test', '[user:null,id:null,]', 'com.ins1st.modular.system.ctrl.SysMenuController', '2019-11-20 08:45:13');
COMMIT;

-- ----------------------------
-- Table structure for sys_upload
-- ----------------------------
DROP TABLE IF EXISTS `sys_upload`;
CREATE TABLE `sys_upload` (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `upload_type` char(1) DEFAULT NULL,
  `file_key` varchar(255) DEFAULT NULL,
  `upload_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(64) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '登入账号',
  `user_nick` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户密码',
  `user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户邮箱',
  `user_mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户电话',
  `user_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户头像',
  `user_sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户性别',
  `login_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否登入 0 可登入 1 不可登入',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES ('1194579339778211841', 'admin', '爸爸', '32ff9ee7e841b26a966870c144fdcaec', '42@qq.com', '17717961296', NULL, '1', '0', '2019-11-13 11:34:43');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(64) NOT NULL COMMENT 'id',
  `user_id` varchar(64) DEFAULT NULL,
  `role_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES ('1194977980250705922', '1194579339778211841', '1194858296071180290');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
