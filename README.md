## 进群获取部署文档

## ins1st-cloud微服务开发平台
* 后端采用Springboot2.X、SpringCloudAlibaba、Mybatis-Plus、Shiro、Redis,并同时对其基础组件做了高度的封装
* hystrix从流量控制、熔断降级、系统负载等多个维度保护服务的稳定性。
* 使用SpringBoot admin全程不间断监控服务，了解服务健康。
* 统一fegin异常管理，根据需求来决定是否走hystrix过滤。
* 注册中心、配置中心选型Nacos，为工程瘦身的同时加强各模块之间的联动。
* 采用JWT做Token认证，可拓展集成Redis等细颗粒度控制方案。
* 项目分包明确，规范微服务的开发模式，使包与包之间的分工清晰。

## 工程结构
``` 
ins1st-cloud
├── ins1st-common -- 常用工具基础封装包
├── ins1st-monitor -- admin监控
├── ins1st-log -- 日志模块
├── ins1st-system -- 系统基础业务模块
├── ins1st-web -- web模块
```
# 开源协议
Apache Licence 2.0 （[英文原文](http://www.apache.org/licenses/LICENSE-2.0.html)）
Apache Licence是著名的非盈利开源组织Apache采用的协议。该协议和BSD类似，同样鼓励代码共享和尊重原作者的著作权，同样允许代码修改，再发布（作为开源或商业软件）。
需要满足的条件如下：
* 需要给代码的用户一份Apache Licence
* 如果你修改了代码，需要在被修改的文件中说明。
* 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，商标，专利声明和其他原来作者规定需要包含的说明。
* 如果再发布的产品中包含一个Notice文件，则在Notice文件中需要带有Apache Licence。你可以在Notice中增加自己的许可，但不可以表现为对Apache Licence构成更改。
Apache Licence也是对商业应用友好的许可。使用者也可以在需要的时候修改代码来满足需要并作为开源或商业产品发布/销售。


## 用户权益
* 允许免费用于学习、毕设、公司项目、私活等。
* 对未经过授权和不遵循 Apache 2.0 协议二次开源或者商业化我们将追究到底。
* `注意`：若禁止条款被发现有权追讨 **19999** 的授权费

## 官网
* 交流一群：`701657098 `

![输入图片说明](https://images.gitee.com/uploads/images/2019/1121/173159_0e46a255_756207.png "E7A6F9A9-C361-40B2-B0DD-6E8131C84A36.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1121/173218_461875ab_756207.png "C10BD76A-E030-4E2A-BC98-CC15BC4A1674.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1121/173242_8ac1ead8_756207.png "68257766-3ACC-44CE-B43E-FE480F38C6D6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1121/173346_1a18c3e2_756207.png "BBFFF115-2723-4E66-86E1-B0F9CC89A47B.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1121/173251_eee47a8b_756207.png "8541AF8A-D07B-4B60-96FA-65FC8C032712.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1121/173305_1a7aa6f6_756207.png "8541AF8A-D07B-4B60-96FA-65FC8C032712.png")

