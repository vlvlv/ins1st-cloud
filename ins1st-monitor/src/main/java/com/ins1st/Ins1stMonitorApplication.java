package com.ins1st;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class Ins1stMonitorApplication {

    private static final Logger log = LoggerFactory.getLogger(Ins1stMonitorApplication.class);


    public static void main(String[] args) {
        SpringApplication.run(Ins1stMonitorApplication.class, args);
        log.info(Ins1stMonitorApplication.class.getSimpleName() + " is start success");
    }

}
